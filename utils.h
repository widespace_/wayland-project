#ifndef _UTILS_H
#define _UTILS_H

#include <unistd.h>

int allocate_shm_file(size_t size);

#endif //_UTILS_H
