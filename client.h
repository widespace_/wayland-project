#ifndef __client_h
#define __client_h

typedef struct keyboard {
	struct xkb_context *xkbCtxt;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_state *xkbState;
} tpKeyboard;

typedef struct state {
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_compositor *compositor;
	struct wl_surface *surface;
	struct wl_buffer *buffer;
	struct wl_shm *shm;
	struct wl_shm_pool *pool;
	struct wl_seat *seat;
	tpKeyboard *keyboard;
	struct xdg_wm_base *wmBase;
	struct xdg_surface *xdgSurface;
	struct xdg_toplevel *toplevel;
} tpState;

#endif //__client_h
