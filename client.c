#include <stdio.h>
#include <wayland-client.h>
#include <string.h>
#include <sys/mman.h>
#include <xkbcommon/xkbcommon.h>
#include <assert.h>

#include "client.h"
#include "utils.h"
#include "xdg-shell.h"

static void glblRegAdd(void *data, struct wl_registry *registry, uint32_t name,
				const char *interface, uint32_t version)
{
	tpState *state = data;
	printf("Interface: %s Name: %u Version: %u\n", interface, name, version);
	if(strcmp(interface, wl_compositor_interface.name) == 0)
	{
		state->compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 4);
		printf("GET\n");
	} else if(strcmp(interface, wl_shm_interface.name) == 0)
	{
		state->shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
		printf("GET\n");
	} else if(strcmp(interface, xdg_wm_base_interface.name) == 0)
	{
		state->wmBase = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
		printf("GET\n");
	} else if(strcmp(interface, wl_seat_interface.name) == 0)
	{
		state->seat = wl_registry_bind(registry, name, &wl_seat_interface, 7);
		printf("GET\n");
	}
}

static void glblRegRem()
{
}

static void wlBufrel(void *data, struct wl_buffer *buf)
{	
	if(data)
	{
		printf("Peepeepoopoo\n");
	}
	wl_buffer_destroy(buf);
}

void drawFrame(tpState *data)
{
	static const int width = 640, height = 480;
	static const int stride = width * 4;
	static const int shmPoolSize = height * stride * 2;

	int fd = allocate_shm_file(shmPoolSize);

	uint32_t *poolData = mmap(NULL, shmPoolSize, 
				  PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	data->pool = wl_shm_create_pool(data->shm, fd, shmPoolSize);
	
	int index = 0;
	int offset = height * stride * index;

	data->buffer = wl_shm_pool_create_buffer(data->pool, offset, width, height, 
						stride, WL_SHM_FORMAT_XRGB8888);

	static const struct wl_buffer_listener wlBufListener = {
		.release = wlBufrel
	};

	wl_buffer_add_listener(data->buffer, &wlBufListener, NULL);

	wl_shm_pool_destroy(data->pool);
	close(fd);

	for(int y = 0; y < height; ++y)
	{
		for(int x = 0; x < width; ++x)
		{
			if((x + y / 8 * 8) % 16 < 8)
				poolData[y * width + x] = 0xFF666666;
			else
				poolData[y * width + x] = 0xFFEEEEEE;
		}
	}

	munmap(poolData, shmPoolSize);
}

void xdgSurfConf(void *data, struct xdg_surface *xdgSurface, uint32_t serial)
{
	printf("CONFIG!\n");
	tpState *state = data;
	xdg_surface_ack_configure(xdgSurface, serial);

	drawFrame(state);

	wl_surface_attach(state->surface, state->buffer, 0, 0);
	wl_surface_damage(state->surface, 0, 0, UINT32_MAX, UINT32_MAX);
	wl_surface_commit(state->surface);
}

void wlCapabilities(void *data, struct wl_seat *seat, uint32_t capability)
{
	if((capability & WL_SEAT_CAPABILITY_KEYBOARD) > 0)
	{
		tpState *state = data;
		state->keyboard->keyboard = wl_seat_get_keyboard(seat);
		printf("KBD GET\n");
	}
}

void wlSeatName()
{
	// Purposefully empty
}

void wlKeymapGet(void *data, struct wl_keyboard *kbrd, uint32_t keymapFmt, int fd, uint32_t size)
{
	assert(keymapFmt == WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1);
	assert(kbrd);

	tpState *state = data;
	
	char *shm = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);

	state->keyboard->keymap = xkb_keymap_new_from_string(state->keyboard->xkbCtxt, shm,
			XKB_KEYMAP_FORMAT_TEXT_V1, XKB_KEYMAP_COMPILE_NO_FLAGS);
	
	munmap(shm, size);
	close(fd);

	printf("FMT GET\n");
}

void wlKbrdEnter()
{
}

void wlKbrdLeave()
{
}

void wlKbrdRptNfo()
{
}

void wlKbrdMod()
{
}

void wlKbrdKey(void *data, struct wl_keyboard *kbrd, uint32_t serial, uint32_t time, 
		uint32_t key, uint32_t state)
{
	tpState *tpData = data;
	xkb_keysym_t keySym = xkb_state_key_get_one_sym(tpData->keyboard->xkbState, key+8);
	if(keySym == XKB_KEY_q && state == WL_KEYBOARD_KEY_STATE_PRESSED)
	{
		printf("Controlled exit\n");
		wl_display_disconnect(tpData->display);
	}
	// A *hopefully* no-op loop to stop the compiler complaining about unused variables
	if(kbrd && time && serial && NULL)
	{
		printf("Stalin was better than Hitler");
	}
}

int main()
{
	tpKeyboard kbd;
	tpState data = {
		.display = wl_display_connect(NULL),
		.keyboard = &kbd,
	};

	if(!data.display)
	{
		fprintf(stderr, "Could not connect to wayland server.\n");
		return 1;
	}
	
	data.registry = wl_display_get_registry(data.display);
	printf("Connected.\n");
	
	static const struct wl_registry_listener registry_listener = {
		.global = glblRegAdd,
		.global_remove = glblRegRem,
	};

	wl_registry_add_listener(data.registry, &registry_listener, &data);
	wl_display_roundtrip(data.display);

	data.surface = wl_compositor_create_surface(data.compositor);


	static const int width = 640, height = 480;
	static const int stride = width * 4;
	static const int shmPoolSize = height * stride * 2;

	int fd = allocate_shm_file(shmPoolSize);

	uint32_t *poolData = mmap(NULL, shmPoolSize, 
				  PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	data.pool = wl_shm_create_pool(data.shm, fd, shmPoolSize);
	
	int index = 0;
	int offset = height * stride * index;

	data.buffer = wl_shm_pool_create_buffer(data.pool, offset, width, height, 
						stride, WL_SHM_FORMAT_XRGB8888);

	static const struct wl_buffer_listener wlBufListener = {
		.release = wlBufrel
	};

	wl_buffer_add_listener(data.buffer, &wlBufListener, NULL);

	wl_shm_pool_destroy(data.pool);
	close(fd);

	for(int y = 0; y < height; ++y)
	{
		for(int x = 0; x < width; ++x)
		{
			if((x + y / 8 * 8) % 16 < 8)
				poolData[y * width + x] = 0xFF666666;
			else
				poolData[y * width + x] = 0xFFEEEEEE;
		}
	}

	munmap(poolData, shmPoolSize);

	data.xdgSurface = xdg_wm_base_get_xdg_surface(data.wmBase, data.surface);

	data.toplevel = xdg_surface_get_toplevel(data.xdgSurface);

	static const struct xdg_surface_listener xdgSurfListener = {
		.configure = xdgSurfConf
	};

	xdg_toplevel_set_title(data.toplevel, "Hello, Window!");

	xdg_surface_add_listener(data.xdgSurface, &xdgSurfListener, &data);

	wl_surface_commit(data.surface);

	static const struct wl_seat_listener wlSeatListener = {
		.name = wlSeatName,
		.capabilities = wlCapabilities
	};

	wl_seat_add_listener(data.seat, &wlSeatListener, &data);

	wl_display_roundtrip(data.display);
	
	if(!data.keyboard)
	{
		fprintf(stderr, "No keyboard found.\n");
		return 1;
	}

	static const struct wl_keyboard_listener wlKbdListener = {
		.keymap = wlKeymapGet,
		.enter = wlKbrdEnter,
		.leave = wlKbrdLeave,
		.repeat_info = wlKbrdRptNfo,
		.modifiers = wlKbrdMod,
		.key = wlKbrdKey,
	};

	data.keyboard->xkbCtxt  = xkb_context_new(XKB_CONTEXT_NO_FLAGS);

	wl_keyboard_add_listener(data.keyboard->keyboard, &wlKbdListener, &data);

	wl_display_roundtrip(data.display);

	data.keyboard->xkbState = xkb_state_new(data.keyboard->keymap);

	while(wl_display_dispatch(data.display) != -1);
	
	if(!data.display)
		wl_display_disconnect(data.display);
	return 0;
}
